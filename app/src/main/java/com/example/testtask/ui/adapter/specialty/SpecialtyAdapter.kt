package com.example.testtask.ui.adapter.specialty

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.testtask.R
import com.example.testtask.data.db.entity.SpecialtyEntity
import com.example.testtask.databinding.ItemSpecialtyBinding

class SpecialtyAdapter : RecyclerView.Adapter<SpecialtyAdapter.SpecialtyViewHolder>() {

    interface OnClickSpecialty {
        fun onClickSpecialty(specialty: Int)
    }

    var listener: OnClickSpecialty? = null

    var mSpecialties: MutableList<SpecialtyEntity> = mutableListOf()

    fun setSpecialties(specialties: List<SpecialtyEntity>) {
        mSpecialties.clear()
        mSpecialties.addAll(specialties)
    }

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): SpecialtyViewHolder {
        val viewBinding: ItemSpecialtyBinding =
                DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_specialty, parent, false)
        return SpecialtyViewHolder(viewBinding, listener)
    }

    override fun getItemCount() = mSpecialties.size

    override fun onBindViewHolder(viewHolder: SpecialtyViewHolder, pos: Int) {
        viewHolder.bind(mSpecialties[pos])
    }

    class SpecialtyViewHolder(
            private val binding: ItemSpecialtyBinding,
            private val listener: OnClickSpecialty?) : RecyclerView.ViewHolder(binding.root) {

        fun bind(specialty: SpecialtyEntity) {
            binding.specialty = specialty
            binding.clRootItemSpecialty.setOnClickListener { listener?.onClickSpecialty(specialty.specialtyId)}
        }
    }
}