package com.example.testtask.ui.fragment.user_info.model

data class UserSpecialty(
    val _id: Int? = null,
    val fName: String,
    val lName: String,
    val birthday: String,
    val age: Int,
    val avatarUrl: String,
    val name: String
)