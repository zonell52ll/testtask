package com.example.testtask.ui.fragment.user_list

import android.os.Bundle
import com.arellomobile.mvp.InjectViewState
import com.example.testtask.data.DataManager
import com.example.testtask.ui.AppTestTask
import com.example.testtask.ui.activity.base.BasePresenter
import com.example.testtask.utils.SPECIALTY
import com.example.testtask.utils.applySchedulers
import javax.inject.Inject

@InjectViewState
class UserListPresenter(private val arguments: Bundle?) : BasePresenter<UserListView>() {

    @Inject
    lateinit var mDataManager: DataManager

    init {
        AppTestTask.mainComponent.inject(this)
    }

    override fun onFirstViewAttach() {
        val specialty = arguments?.getInt(SPECIALTY)
        specialty?.let {
            mDataManager.getUserForSpecialty(it)
                    .applySchedulers()
                    .rxSubscribe({users ->
                        viewState.hideProgressBar()
                        viewState.initAdapter(users)
                    })
        }
    }
}