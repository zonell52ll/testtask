package com.example.testtask.ui.fragment.user_list

import android.support.v7.util.DiffUtil
import android.support.v7.widget.LinearLayoutManager
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.example.testtask.R
import com.example.testtask.data.db.entity.UserEntity
import com.example.testtask.ui.activity.base.BaseMvpFragment
import com.example.testtask.ui.adapter.userList.UserListAdapter
import com.example.testtask.ui.adapter.userList.UserListDiffUtilCallback
import com.example.testtask.utils.SPECIALTY
import com.example.testtask.utils.gone
import kotlinx.android.synthetic.main.fragment_user_list.*
import org.jetbrains.anko.bundleOf

class UserListFragment : BaseMvpFragment(), UserListView, UserListAdapter.OnClickedUser {

    override fun getLayoutId() = R.layout.fragment_user_list

    @InjectPresenter
    lateinit var mUserListPresenter: UserListPresenter

    @ProvidePresenter
    fun provideUserListPresenter() = UserListPresenter(arguments)

    companion object {
        fun getInstance(specialty: Int) = UserListFragment().apply {
            arguments = bundleOf(SPECIALTY to specialty)
        }
    }

    private val mUserListAdapter: UserListAdapter by lazy { UserListAdapter() }
    var userSelectedListener: OnUserSelected? = null

    interface OnUserSelected {
        fun onUserSelected(userId: String)
    }

    override fun initUi() {
        initUserListAdapter()
    }

    override fun initAdapter(users: List<UserEntity>) {
        val diffUtils = UserListDiffUtilCallback(mUserListAdapter.mUserList, users)
        val diffResult = DiffUtil.calculateDiff(diffUtils)
        mUserListAdapter.setUserList(users)
        diffResult.dispatchUpdatesTo(mUserListAdapter)
    }

    override fun onClickedUser(id: String) {
        userSelectedListener?.onUserSelected(id)
    }

    override fun hideProgressBar() {
        pbUserListFragment.gone()
    }

    private fun initUserListAdapter() {
        val layoutManager = LinearLayoutManager(requireContext())
        rvUserList.layoutManager = layoutManager
        rvUserList.adapter = mUserListAdapter
        mUserListAdapter.listener = this
    }
}