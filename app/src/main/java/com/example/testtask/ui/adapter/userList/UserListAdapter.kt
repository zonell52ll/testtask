package com.example.testtask.ui.adapter.userList

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.testtask.R
import com.example.testtask.data.db.entity.UserEntity
import com.example.testtask.databinding.ItemUserListBinding

class UserListAdapter : RecyclerView.Adapter<UserListAdapter.UserListViewHolder>() {

    interface OnClickedUser {
        fun onClickedUser(id: String)
    }

    var listener: OnClickedUser? = null

    var mUserList: MutableList<UserEntity> = mutableListOf()

    fun setUserList(userList: List<UserEntity>) {
        mUserList.clear()
        mUserList.addAll(userList)
    }

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): UserListViewHolder {
        val viewBinding: ItemUserListBinding =
                DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_user_list, parent, false)
        return UserListViewHolder(viewBinding, listener)
    }

    override fun getItemCount() = mUserList.size

    override fun onBindViewHolder(viewHolder: UserListViewHolder, pos: Int) {
        viewHolder.bind(mUserList[pos])
    }

    class UserListViewHolder(
            private val binding: ItemUserListBinding,
            private val listener: OnClickedUser?) : RecyclerView.ViewHolder(binding.root) {

        fun bind(user: UserEntity) {
            binding.user = user
            binding.clRootItemUser.setOnClickListener {
                listener?.onClickedUser(user._id)
            }
        }
    }
}