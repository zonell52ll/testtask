package com.example.testtask.ui.fragment.user_info

import android.os.Bundle
import com.arellomobile.mvp.InjectViewState
import com.example.testtask.data.DataManager
import com.example.testtask.ui.AppTestTask
import com.example.testtask.ui.activity.base.BasePresenter
import com.example.testtask.ui.fragment.user_info.model.UserSpecialty
import com.example.testtask.utils.EMPTY_STRING
import com.example.testtask.utils.USER
import com.example.testtask.utils.applySchedulers
import java.lang.StringBuilder
import javax.inject.Inject

@InjectViewState
class UserInfoPresenter(private val argument: Bundle?) : BasePresenter<UserInfoView>() {

    @Inject
    lateinit var mDataManager: DataManager

    init {
        AppTestTask.mainComponent.inject(this)
    }

    companion object {
        const val FIRST_ITEM = 0
    }

    override fun onFirstViewAttach() {
        argument?.getString(USER)?.let {
            mDataManager.getUserInfo(it)
                    .applySchedulers()
                    .rxSubscribe({userSpecialty ->
                        viewState.hideProgressBar()
                        viewState.setUserInfo(getUserSpecialty(userSpecialty))
                    })
        }
    }

    private fun getUserSpecialty(list: List<UserSpecialty>): UserSpecialty {
        val specialty = StringBuilder()
        for (pos in list.indices) {
            specialty.append(list[pos].name).append(EMPTY_STRING)
        }
        return UserSpecialty(
                fName = list[FIRST_ITEM].fName,
                lName = list[FIRST_ITEM].lName,
                birthday = list[FIRST_ITEM].birthday,
                age = list[FIRST_ITEM].age,
                avatarUrl = list[FIRST_ITEM].avatarUrl,
                name = specialty.toString()
        )
    }
}