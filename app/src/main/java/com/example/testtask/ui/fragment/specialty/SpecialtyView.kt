package com.example.testtask.ui.fragment.specialty

import com.example.testtask.data.db.entity.SpecialtyEntity
import com.example.testtask.ui.activity.base.view.BaseMvpView

interface SpecialtyView : BaseMvpView {
    fun initAdapter(specialties: List<SpecialtyEntity>)
    fun hideProgressBar()
}