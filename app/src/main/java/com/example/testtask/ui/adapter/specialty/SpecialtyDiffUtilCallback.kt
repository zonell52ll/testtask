package com.example.testtask.ui.adapter.specialty

import android.support.v7.util.DiffUtil
import com.example.testtask.data.db.entity.SpecialtyEntity

class SpecialtyDiffUtilCallback(
        private val oldList: List<SpecialtyEntity>,
        private val newList: List<SpecialtyEntity>) : DiffUtil.Callback() {

    override fun getOldListSize() = oldList.size

    override fun getNewListSize() = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldSpecialty = oldList[oldItemPosition]
        val newSpecialty = newList[newItemPosition]
        return oldSpecialty.name == newSpecialty.name
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldSpecialty = oldList[oldItemPosition]
        val newSpecialty = newList[newItemPosition]
        return oldSpecialty.name == newSpecialty.name
    }
}