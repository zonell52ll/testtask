package com.example.testtask.ui.fragment.specialty

import android.support.v7.util.DiffUtil
import android.support.v7.widget.LinearLayoutManager
import com.arellomobile.mvp.presenter.InjectPresenter
import com.example.testtask.R
import com.example.testtask.data.db.entity.SpecialtyEntity
import com.example.testtask.ui.activity.base.BaseMvpFragment
import com.example.testtask.ui.adapter.specialty.SpecialtyAdapter
import com.example.testtask.ui.adapter.specialty.SpecialtyDiffUtilCallback
import com.example.testtask.utils.gone
import kotlinx.android.synthetic.main.fragment_specialty.*

class SpecialtyFragment : BaseMvpFragment(), SpecialtyView, SpecialtyAdapter.OnClickSpecialty {

    override fun getLayoutId() = R.layout.fragment_specialty

    @InjectPresenter
    lateinit var mSpecialtyPresenter: SpecialtyPresenter

    var specialtySelectedListener: OnSpecialtySelected? = null
    private val mSpecialtyAdapter: SpecialtyAdapter by lazy { SpecialtyAdapter() }

    interface OnSpecialtySelected {
        fun onSpecialtySelected(specialty: Int)
    }

    override fun initUi() {
        initContactRecycler()
    }

    private fun initContactRecycler() {
        val layoutManager = LinearLayoutManager(requireContext())
        rvSpecialty.layoutManager = layoutManager
        rvSpecialty.adapter = mSpecialtyAdapter
        mSpecialtyAdapter.listener = this
    }

    override fun initAdapter(specialties: List<SpecialtyEntity>) {
        val diffUtilCallback = SpecialtyDiffUtilCallback(mSpecialtyAdapter.mSpecialties, specialties)
        val diffResult = DiffUtil.calculateDiff(diffUtilCallback)
        mSpecialtyAdapter.setSpecialties(specialties)
        diffResult.dispatchUpdatesTo(mSpecialtyAdapter)
    }

    override fun onClickSpecialty(specialty: Int) {
        specialtySelectedListener?.onSpecialtySelected(specialty)
    }

    override fun hideProgressBar() {
        pbSpecialtyFragment.gone()
    }
}
