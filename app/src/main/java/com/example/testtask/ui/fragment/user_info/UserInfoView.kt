package com.example.testtask.ui.fragment.user_info

import com.example.testtask.ui.activity.base.view.BaseMvpView
import com.example.testtask.ui.fragment.user_info.model.UserSpecialty

interface UserInfoView : BaseMvpView {
    fun setUserInfo(user: UserSpecialty)
    fun hideProgressBar()
}