package com.example.testtask.ui.adapter.userList

import android.support.v7.util.DiffUtil
import com.example.testtask.data.db.entity.UserEntity

class UserListDiffUtilCallback(
        private val oldList: List<UserEntity>,
        private val newList: List<UserEntity>) : DiffUtil.Callback() {

    override fun getOldListSize() = oldList.size

    override fun getNewListSize() = newList.size

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldUser = oldList[oldItemPosition]
        val newUser = newList[newItemPosition]
        return oldUser.fName == newUser.fName
    }

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldUser = oldList[oldItemPosition]
        val newUser = newList[newItemPosition]
        return oldUser.fName == newUser.fName
    }
}