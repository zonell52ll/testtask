package com.example.testtask.ui.activity.base

import com.arellomobile.mvp.MvpPresenter
import com.arellomobile.mvp.MvpView
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import java.net.UnknownHostException
import java.util.concurrent.TimeoutException

open class BasePresenter<T : MvpView> : MvpPresenter<T>(){
    private val compositeDisposable: CompositeDisposable = CompositeDisposable()

    override fun onDestroy() {
        compositeDisposable.dispose()
        super.onDestroy()
    }

    fun getProperErrorDescription(throwable: Throwable?): String {
        return when (throwable) {
            is UnknownHostException -> "The internet connection appears to be offline"
            is TimeoutException -> "Can't connect to server (timeout)"
            else -> throwable?.message ?: "Unknown error"
        }
    }

    private fun Disposable.rxDispose() {
        compositeDisposable.add(this)
    }

    /**
     * Subscribe to source and dispose it automatically
     */
    protected fun <T> Single<T>.rxSubscribe(onSuccess: (T) -> Unit = {}, onError: (Throwable) -> Unit = {}) {
        this.subscribe({
            onSuccess(it)
        }, {
            onError(it)
        }).rxDispose()
    }

    /**
     * Subscribe to source and dispose it automatically
     */
    protected fun Completable.rxSubscribe(onSuccess: () -> Unit = {}, onError: (Throwable) -> Unit = {}) {
        this.subscribe({
            onSuccess()
        }, {
            onError(it)
        }).rxDispose()
    }

    /**
     * Subscribe to source and dispose it automatically
     */
    protected fun <T> Observable<T>.rxSubscribe(onNext: (T) -> Unit = {}, onError: (Throwable) -> Unit = {},
                                                onSuccess: () -> Unit = {}) {
        this.subscribe({
            onNext(it)
        }, {
            onError(it)
        }, {
            onSuccess()
        }
        ).rxDispose()
    }

    /**
     * Subscribe to source and dispose it automatically
     */
    protected fun <T> Flowable<T>.rxSubscribe(onNext: (T) -> Unit = {}, onError: (Throwable) -> Unit = {},
                                                onSuccess: () -> Unit = {}) {
        this.subscribe({
            onNext(it)
        }, {
            onError(it)
        }, {
            onSuccess()
        }
        ).rxDispose()
    }
}