package com.example.testtask.ui.activity.base

import android.content.Context
import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.os.Bundle
import android.support.annotation.StringRes
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.MvpAppCompatFragment
import com.example.testtask.ui.activity.base.view.BaseMvpView

abstract class BaseMvpFragment : MvpAppCompatFragment(), BaseMvpView {

    private lateinit var mBaseActivity: BaseMvpView
    protected lateinit var viewBinding: ViewDataBinding

    abstract fun getLayoutId() : Int

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewBinding = DataBindingUtil.inflate(inflater, getLayoutId(), container, false)
        return viewBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUi()
    }

    override fun onAttach(context: Context) {
        mBaseActivity = context as BaseMvpView
        super.onAttach(context)
    }

    open fun initUi() {}

    override fun showMessage(message: String) {
        mBaseActivity.showMessage(message)
    }

    override fun showMessage(@StringRes resId: Int) {
        mBaseActivity.showMessage(resId)
    }
}