package com.example.testtask.ui.fragment.specialty

import com.arellomobile.mvp.InjectViewState
import com.example.testtask.data.DataManager
import com.example.testtask.ui.AppTestTask
import com.example.testtask.ui.activity.base.BasePresenter
import com.example.testtask.utils.applySchedulers
import javax.inject.Inject

@InjectViewState
class SpecialtyPresenter : BasePresenter<SpecialtyView>() {

    @Inject
    lateinit var mAppDataManager: DataManager

    init {
        AppTestTask.mainComponent.inject(this)
    }

    override fun onFirstViewAttach() {
        mAppDataManager.getAllSpecialty()
            .applySchedulers()
            .rxSubscribe({
                viewState.hideProgressBar()
                viewState.initAdapter(it)
            })
    }
}