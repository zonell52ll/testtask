package com.example.testtask.ui

import android.app.Application
import com.example.testtask.di.AppComponent
import com.example.testtask.di.DaggerAppComponent
import com.example.testtask.di.module.AppModule

class AppTestTask : Application() {

    companion object {
        lateinit var mainComponent: AppComponent
        private set
    }

    override fun onCreate() {
        super.onCreate()
        mainComponent = initDagger()
    }

    private fun initDagger(): AppComponent = DaggerAppComponent.builder()
            .appModule(AppModule(this)).build()
}