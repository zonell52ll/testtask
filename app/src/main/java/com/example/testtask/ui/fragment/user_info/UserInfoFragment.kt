package com.example.testtask.ui.fragment.user_info

import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.example.testtask.R
import com.example.testtask.databinding.FragmentUserInfoBinding
import com.example.testtask.ui.activity.base.BaseMvpFragment
import com.example.testtask.ui.fragment.user_info.model.UserSpecialty
import com.example.testtask.utils.USER
import com.example.testtask.utils.gone
import kotlinx.android.synthetic.main.fragment_user_info.*
import org.jetbrains.anko.bundleOf

class UserInfoFragment : BaseMvpFragment(), UserInfoView {

    companion object {
        fun getInstance(user: String) = UserInfoFragment().apply {
            arguments = bundleOf(USER to user)
        }
    }

    @InjectPresenter
    lateinit var mUserInfoPresenter: UserInfoPresenter

    @ProvidePresenter
    fun provideUserInfoPresenter() = UserInfoPresenter(arguments)

    private val binding: FragmentUserInfoBinding by lazy { viewBinding as FragmentUserInfoBinding }

    override fun getLayoutId() = R.layout.fragment_user_info

    override fun setUserInfo(user: UserSpecialty) {
        binding.user = user
    }

    override fun hideProgressBar() {
        pbUserInfoFragment.gone()
    }
}