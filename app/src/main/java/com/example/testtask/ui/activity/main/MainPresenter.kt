package com.example.testtask.ui.activity.main

import com.arellomobile.mvp.InjectViewState
import com.example.testtask.data.DataManager
import com.example.testtask.data.db.entity.SpecialtyEntity
import com.example.testtask.data.db.entity.UserEntity
import com.example.testtask.data.db.entity.UserWithSpecialty
import com.example.testtask.data.network.response.main_data.Response
import com.example.testtask.ui.AppTestTask
import com.example.testtask.ui.activity.base.BasePresenter
import com.example.testtask.utils.ERROR
import com.example.testtask.utils.ONE_FRAGMENT
import com.example.testtask.utils.applySchedulers
import io.reactivex.schedulers.Schedulers
import java.util.*
import javax.inject.Inject

@InjectViewState
class MainPresenter : BasePresenter<MainView>() {

    @Inject
    lateinit var mAppDataManager: DataManager

    init {
        AppTestTask.mainComponent.inject(this)
    }

    override fun onFirstViewAttach() {
        mAppDataManager.getJSON()
            .subscribeOn(Schedulers.io())
            .rxSubscribe({
                if (it.response.isNotEmpty()) {
                    saveAndShowData(it.response)
                }
            }, {
                it.printStackTrace()
                viewState.showMessage(it.message?: ERROR)
            })
    }

    fun showUserList(specialty: Int) {
        viewState.showUserList(specialty)
    }

    fun showUserInfo(id: String) {
        viewState.showUserInfo(id)
    }

    fun onBackClicked(count: Int) {
        if (count > ONE_FRAGMENT) {
            viewState.goToBack()
        } else {
            viewState.exit()
        }
    }

    private fun saveAndShowData(response: List<Response>) {
        val listSpecialty = mutableListOf<SpecialtyEntity>()
        val listUser = mutableListOf<UserEntity>()
        val listUserWithSpecialty = mutableListOf<UserWithSpecialty>()
        for (pos in response.indices) {
            val id = UUID.randomUUID().toString()
            listUser.add(response[pos].toUser(id))

            for (posUserWithSpecialty in response[pos].specialty.indices) {
                val userWithSpecialty = UserWithSpecialty(
                    userId = id,
                    specialtyId = response[pos].specialty[posUserWithSpecialty].specialty_id
                )
                listUserWithSpecialty.add(userWithSpecialty)

                val specialty  = SpecialtyEntity(
                    response[pos].specialty[posUserWithSpecialty].specialty_id,
                    response[pos].specialty[posUserWithSpecialty].name
                )
                listSpecialty.add(specialty)
            }
        }

        mAppDataManager.saveSpecialty(listSpecialty).subscribeOn(Schedulers.io()).subscribe()
        mAppDataManager.replaceUserWithSpecialty(listUserWithSpecialty).subscribeOn(Schedulers.io()).subscribe()

        mAppDataManager.replaceUser(listUser)
                .applySchedulers()
                .rxSubscribe({
                    viewState.showSpecialty()
                })
    }
}