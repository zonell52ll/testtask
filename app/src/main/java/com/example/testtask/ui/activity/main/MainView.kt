package com.example.testtask.ui.activity.main

import com.example.testtask.ui.activity.base.view.BaseMvpView

interface MainView : BaseMvpView {
    fun showSpecialty()
    fun showUserList(specialty: Int)
    fun showUserInfo(userId: String)
    fun goToBack()
    fun exit()
}