package com.example.testtask.ui.fragment.user_list

import com.example.testtask.data.db.entity.UserEntity
import com.example.testtask.ui.activity.base.view.BaseMvpView

interface UserListView : BaseMvpView {
    fun initAdapter(users: List<UserEntity>)
    fun hideProgressBar()
}