package com.example.testtask.ui.activity.base

import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.os.Bundle
import android.support.annotation.StringRes
import com.arellomobile.mvp.MvpAppCompatActivity
import com.example.testtask.ui.activity.base.view.BaseMvpView
import org.jetbrains.anko.longToast

abstract class BaseMvpActivity : MvpAppCompatActivity(), BaseMvpView {

    protected lateinit var viewBinding: ViewDataBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getLayoutId())
    }

    open fun initUi() {}

    abstract fun getLayoutId(): Int

    override fun setContentView(layoutResID: Int) {
        viewBinding = DataBindingUtil.inflate(layoutInflater, layoutResID, null, false)
        super.setContentView(viewBinding.root)
        initUi()
    }

    override fun showMessage(message: String) {
        longToast(message)
    }

    override fun showMessage(@StringRes resId: Int) {
        showMessage(getString(resId))
    }
}