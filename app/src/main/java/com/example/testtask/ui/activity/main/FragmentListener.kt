package com.example.testtask.ui.activity.main

import com.example.testtask.ui.fragment.specialty.SpecialtyFragment
import com.example.testtask.ui.fragment.user_list.UserListFragment

interface FragmentListener :  SpecialtyFragment.OnSpecialtySelected, UserListFragment.OnUserSelected