package com.example.testtask.ui.adapter.binding

import android.databinding.BindingAdapter
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.testtask.R
import com.example.testtask.utils.isAgeEmpty
import com.bumptech.glide.request.RequestOptions

/**TextView*/
@BindingAdapter("setAge")
fun TextView.setAge(age: Int) {
    text = when {
        age.isAgeEmpty() -> context.getString(R.string.empty_age)
        else -> this.context.resources.getQuantityString(R.plurals.plurals_year, age, age)
    }
}

/**ImageView*/
@BindingAdapter("setAvatar")
fun ImageView.setAvatar(url: String?) {
    val options = RequestOptions()
            .centerCrop()
            .placeholder(R.drawable.ic_no_avatar)
            .error(R.drawable.ic_no_avatar)
            .diskCacheStrategy(DiskCacheStrategy.ALL)

    Glide.with(this)
            .load(url)
            .apply(options)
            .into(this)
}