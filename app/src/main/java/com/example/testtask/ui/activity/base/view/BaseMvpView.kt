package com.example.testtask.ui.activity.base.view

import android.support.annotation.StringRes
import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

@StateStrategyType(AddToEndSingleStrategy :: class)
interface BaseMvpView : MvpView{
    fun showMessage(message: String)
    fun showMessage(@StringRes resId: Int)
}