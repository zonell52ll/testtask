package com.example.testtask.ui.activity.main

import android.support.v4.app.Fragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.example.testtask.R
import com.example.testtask.ui.activity.base.BaseMvpActivity
import com.example.testtask.ui.fragment.specialty.SpecialtyFragment
import com.example.testtask.ui.fragment.user_info.UserInfoFragment
import com.example.testtask.ui.fragment.user_list.UserListFragment

class MainActivity : BaseMvpActivity(), MainView, FragmentListener {

    override fun getLayoutId() = R.layout.activity_main

    @InjectPresenter
    lateinit var mMainPresenter: MainPresenter

    override fun showSpecialty() {
        val specialtyFragment = supportFragmentManager.findFragmentByTag(SpecialtyFragment().javaClass.name)
        if (specialtyFragment != null) {
            loadFragment(specialtyFragment)
        } else {
            loadFragment(SpecialtyFragment())
        }
    }

    override fun onSpecialtySelected(specialty: Int) {
        mMainPresenter.showUserList(specialty)
    }

    override fun onAttachFragment(fragment: Fragment?) {
        super.onAttachFragment(fragment)
        when (fragment) {
            is SpecialtyFragment -> fragment.specialtySelectedListener = this
            is UserListFragment -> fragment.userSelectedListener = this
        }
    }

    override fun showUserList(specialty: Int) {
        val userListFragment = supportFragmentManager.findFragmentByTag(UserListFragment().javaClass.name)
        if (userListFragment != null) {
            loadFragment(userListFragment)
        } else {
            loadFragment(UserListFragment.getInstance(specialty))
        }
    }

    override fun onUserSelected(userId: String) {
        mMainPresenter.showUserInfo(userId)
    }

    override fun showUserInfo(userId: String) {
        val userInfo = supportFragmentManager.findFragmentByTag(UserListFragment().javaClass.name)
        if (userInfo != null) {
            loadFragment(userInfo)
        } else {
            loadFragment(UserInfoFragment.getInstance(userId))
        }
    }

    override fun goToBack() {
        super.onBackPressed()
    }

    override fun exit() {
        finish()
    }

    private fun loadFragment(fragment: Fragment) {
        val ft = supportFragmentManager.beginTransaction()
        val fragmentName = fragment.javaClass.simpleName
        ft.replace(R.id.flMainContent, fragment, fragmentName)
        ft.addToBackStack(fragmentName)
        ft.commit()
    }

    override fun onBackPressed() {
        mMainPresenter.onBackClicked(supportFragmentManager.backStackEntryCount)
    }
}
