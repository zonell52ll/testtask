package com.example.testtask.data.network.response.main_data

import com.google.gson.annotations.SerializedName

data class MainJSON(
    @SerializedName(RESPONSE)
    var response: List<Response>
) {
    companion object {
        const val RESPONSE = "response"
    }
}