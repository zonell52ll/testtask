package com.example.testtask.data.db

import com.example.testtask.data.db.entity.SpecialtyEntity
import com.example.testtask.data.db.entity.UserEntity
import com.example.testtask.data.db.entity.UserWithSpecialty
import com.example.testtask.ui.fragment.user_info.model.UserSpecialty
import io.reactivex.Completable
import io.reactivex.Single

interface DBHelper {

    /**User*/
    fun getUserForSpecialty(specialty: Int): Single<List<UserEntity>>
    fun getUserById(userId: String): Single<UserEntity>
    fun replaceUser(users: List<UserEntity>) : Completable
    fun getUserInfo(userId: String): Single<List<UserSpecialty>>

    /**Specialty*/
    fun getAllSpecialty(): Single<List<SpecialtyEntity>>
    fun saveSpecialty(specialties: List<SpecialtyEntity>) : Completable
    fun deleteSpecialty() : Completable
    fun getSpecialtyUser(userId: String): Single<List<String>>

    /**UserWithSpecialty*/
    fun replaceUserWithSpecialty(userWithSpecialty: List<UserWithSpecialty>): Completable
}