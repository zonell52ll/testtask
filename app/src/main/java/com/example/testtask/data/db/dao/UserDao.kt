package com.example.testtask.data.db.dao

import android.arch.persistence.room.*
import com.example.testtask.data.db.entity.UserEntity
import com.example.testtask.ui.fragment.user_info.model.UserSpecialty
import io.reactivex.Single

@Dao
interface UserDao {
    @Query("SELECT UserEntity._id, UserEntity.fName, UserEntity.lName, UserEntity.birthday, UserEntity.age, UserEntity.avatarUrl FROM UserEntity INNER JOIN UserWithSpecialty ON UserEntity._id = UserWithSpecialty.userId WHERE UserWithSpecialty.specialtyId = :specialty")
    fun getUserForSpecialty(specialty: Int): Single<List<UserEntity>>

    @Query("select US._id, US.fName, US.lName, US.birthday, US.age, US.avatarUrl, SP.name from (SELECT * FROM UserEntity WHERE _id = :userId) as US, (select * from SpecialtyEntity inner join UserWithSpecialty on SpecialtyEntity.specialtyId = UserWithSpecialty.specialtyId and UserWithSpecialty.userId = :userId) as SP")
    fun getUserInfo(userId: String): Single<List<UserSpecialty>>

    @Query("SELECT * FROM UserEntity WHERE _id = :userId")
    fun getUserById(userId: String): Single<UserEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveUser(users: List<UserEntity>)

    @Query("DELETE FROM UserEntity")
    fun deleteUser()

    @Transaction
    fun replace(users: List<UserEntity>) {
        deleteUser()
        saveUser(users)
    }
}