package com.example.testtask.data.network

import com.example.testtask.data.network.response.main_data.MainJSON
import com.example.testtask.di.module.NetworkModule
import io.reactivex.Single
import retrofit2.http.GET

interface Api {

    @GET(NetworkModule.MAIN_DATA)
    fun getJSON(): Single<MainJSON>
}