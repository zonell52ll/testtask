package com.example.testtask.data.db.entity
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity
class UserWithSpecialty(
    @PrimaryKey(autoGenerate = true)
    val id: Int? = null,
    val userId: String,
    val specialtyId: Int
)