package com.example.testtask.data.network.response.main_data

import com.google.gson.annotations.SerializedName

data class Specialty(
    @SerializedName(NAME)
    var name: String,
    @SerializedName(SPECIALTY_ID)
    var specialty_id: Int
) {
    companion object {
        const val NAME = "name"
        const val SPECIALTY_ID = "specialty_id"
    }
}