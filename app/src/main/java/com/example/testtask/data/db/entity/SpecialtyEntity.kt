package com.example.testtask.data.db.entity

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity
class SpecialtyEntity(
        @PrimaryKey
        @ColumnInfo(name = SPECIALTY_ID)
        val specialtyId: Int,
        @ColumnInfo(name = NAME)
        val name: String
) {
    companion object {
        const val SPECIALTY_ID = "specialtyId"
        const val NAME = "name"
    }
}