package com.example.testtask.data.db

import com.example.testtask.data.db.entity.SpecialtyEntity
import com.example.testtask.data.db.entity.UserEntity
import com.example.testtask.data.db.entity.UserWithSpecialty
import io.reactivex.Completable
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DBManager
@Inject
constructor(private val mAppDataBase: AppDataBase) : DBHelper {

    /**User*/
    override fun getUserForSpecialty(specialty: Int) = mAppDataBase.getUserDao().getUserForSpecialty(specialty)
    override fun getUserById(userId: String) = mAppDataBase.getUserDao().getUserById(userId)
    override fun replaceUser(users: List<UserEntity>)= Completable.fromAction {
        mAppDataBase.getUserDao().replace(users)
    }

    override fun getUserInfo(userId: String) = mAppDataBase.getUserDao().getUserInfo(userId)

    /**Specialty*/
    override fun getAllSpecialty() = mAppDataBase.getSpecialtyDao().getAllSpecialty()
    override fun getSpecialtyUser(userId: String) = mAppDataBase.getSpecialtyDao().getSpecialtyUser(userId)
    override fun saveSpecialty(specialties: List<SpecialtyEntity>) = Completable.fromAction {
        mAppDataBase.getSpecialtyDao().saveSpecialty(specialties)
    }
    override fun deleteSpecialty() = Completable.fromAction {
        mAppDataBase.getSpecialtyDao().deleteSpecialty()
    }

    override fun replaceUserWithSpecialty(userWithSpecialty: List<UserWithSpecialty>)= Completable.fromAction {
        mAppDataBase.getUserWithSpecialtyDao().replaceUserWithSpecialty(userWithSpecialty)
    }
}