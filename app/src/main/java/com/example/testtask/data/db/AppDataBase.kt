package com.example.testtask.data.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.example.testtask.data.db.dao.SpecialtyDao
import com.example.testtask.data.db.dao.UserDao
import com.example.testtask.data.db.dao.UserWithSpecialtyDao
import com.example.testtask.data.db.entity.SpecialtyEntity
import com.example.testtask.data.db.entity.UserEntity
import com.example.testtask.data.db.entity.UserWithSpecialty

@Database(
    entities = [
        UserEntity::class,
        SpecialtyEntity::class,
    UserWithSpecialty::class
    ],
    version = 1,
    exportSchema = false
)

abstract class AppDataBase : RoomDatabase() {
    abstract fun getUserDao(): UserDao
    abstract fun getSpecialtyDao() : SpecialtyDao
    abstract fun getUserWithSpecialtyDao() : UserWithSpecialtyDao
}