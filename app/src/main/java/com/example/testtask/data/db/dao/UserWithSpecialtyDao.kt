package com.example.testtask.data.db.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import android.arch.persistence.room.Transaction
import com.example.testtask.data.db.entity.UserWithSpecialty

@Dao
interface UserWithSpecialtyDao {

    @Insert
    fun saveUserWithSpecialty(userWithSpecialty: List<UserWithSpecialty>)

    @Query("DELETE FROM UserWithSpecialty")
    fun deleteUserWithSpecialty()

    @Transaction
    fun replaceUserWithSpecialty(userWithSpecialty: List<UserWithSpecialty>) {
        deleteUserWithSpecialty()
        saveUserWithSpecialty(userWithSpecialty)
    }
}