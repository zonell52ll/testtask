package com.example.testtask.data.network

import com.example.testtask.data.network.response.main_data.MainJSON
import io.reactivex.Single

interface NetworkHelper {

    fun getJSON() : Single<MainJSON>
}