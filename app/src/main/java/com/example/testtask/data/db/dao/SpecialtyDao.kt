package com.example.testtask.data.db.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.example.testtask.data.db.entity.SpecialtyEntity
import io.reactivex.Single

@Dao
interface SpecialtyDao {
    @Query("SELECT * FROM SpecialtyEntity")
    fun getAllSpecialty(): Single<List<SpecialtyEntity>>

    @Query("SELECT name FROM SpecialtyEntity, UserWithSpecialty WHERE UserWithSpecialty.userId = :userId")
    fun getSpecialtyUser(userId: String): Single<List<String>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveSpecialty(specialties: List<SpecialtyEntity>)

    @Query("DELETE FROM UserEntity")
    fun deleteSpecialty()
}