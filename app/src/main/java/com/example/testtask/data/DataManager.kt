package com.example.testtask.data

import com.example.testtask.data.db.DBHelper
import com.example.testtask.data.network.NetworkHelper

interface DataManager : DBHelper, NetworkHelper