package com.example.testtask.data.network.response.main_data

import com.example.testtask.data.db.entity.UserEntity
import com.example.testtask.utils.parseAge
import com.example.testtask.utils.parseDate
import com.google.gson.annotations.SerializedName

data class Response(
    @SerializedName(AVATR_URL)
    var avatr_url: String?,
    @SerializedName(BIRTHDAY)
    var birthday: String?,
    @SerializedName(FIRST_NAME)
    var firstName: String,
    @SerializedName(LAST_NAME)
    var lastName: String,
    @SerializedName(SPECIALTY)
    var specialty: List<Specialty>
) {
    companion object {
        const val AVATR_URL = "avatr_url"
        const val BIRTHDAY = "birthday"
        const val FIRST_NAME = "f_name"
        const val LAST_NAME = "l_name"
        const val SPECIALTY = "specialty"

        const val EMPTY_AVATAR = ""
    }

    fun toUser(id: String) = UserEntity(
        _id = id,
        fName = firstName.toLowerCase().capitalize(),
        lName = lastName.toLowerCase().capitalize(),
        birthday = birthday.parseDate(),
        age = birthday.parseAge(),
        avatarUrl = avatr_url ?: EMPTY_AVATAR
    )
}