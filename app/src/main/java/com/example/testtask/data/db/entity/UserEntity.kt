package com.example.testtask.data.db.entity

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity
class UserEntity(
        @PrimaryKey
        @ColumnInfo(name = ID)
        var _id: String,
        @ColumnInfo(name = FIRST_NAME)
        var fName: String,
        @ColumnInfo(name = LAST_NAME)
        var lName: String,
        @ColumnInfo(name = BIRTHDAY)
        var birthday: String,
        @ColumnInfo(name = AGE)
        var age: Int,
        @ColumnInfo(name = AVATR_URL)
        var avatarUrl: String
) {
    companion object {
        const val ID = "_id"
        const val AVATR_URL = "avatarUrl"
        const val BIRTHDAY = "birthday"
        const val FIRST_NAME = "fName"
        const val LAST_NAME = "lName"
        const val AGE = "age"
    }
}