package com.example.testtask.data

import com.example.testtask.data.db.DBHelper
import com.example.testtask.data.db.entity.SpecialtyEntity
import com.example.testtask.data.db.entity.UserEntity
import com.example.testtask.data.db.entity.UserWithSpecialty
import com.example.testtask.data.network.NetworkHelper
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AppDataManager
@Inject
constructor(
        private val mDBHelper: DBHelper,
        private val mNetworkHelper: NetworkHelper
) : DataManager {

    //DataBase section
    /**User*/
    override fun getUserForSpecialty(specialty: Int) = mDBHelper.getUserForSpecialty(specialty)
    override fun getUserById(userId: String) = mDBHelper.getUserById(userId)
    override fun replaceUser(users: List<UserEntity>) = mDBHelper.replaceUser(users)
    override fun getUserInfo(userId: String) = mDBHelper.getUserInfo(userId)
    /**Specialty*/
    override fun getAllSpecialty() = mDBHelper.getAllSpecialty()
    override fun getSpecialtyUser(userId: String) = mDBHelper.getSpecialtyUser(userId)
    override fun saveSpecialty(specialties: List<SpecialtyEntity>) = mDBHelper.saveSpecialty(specialties)
    override fun deleteSpecialty() = mDBHelper.deleteSpecialty()

    /**UserWithSpecialty*/
    override fun replaceUserWithSpecialty(userWithSpecialty: List<UserWithSpecialty>) = mDBHelper.replaceUserWithSpecialty(userWithSpecialty)

    //Network section
    override fun getJSON() = mNetworkHelper.getJSON()
}