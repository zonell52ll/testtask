package com.example.testtask.data.network

import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class NetworkManager
@Inject
constructor(private val mApi: Api) : NetworkHelper {

    override fun getJSON() = mApi.getJSON()
}
