package com.example.testtask.utils

import android.view.View
import java.text.SimpleDateFormat
import java.util.*

/**String*/
private fun getSimpleDateFormat(pattern : String) = SimpleDateFormat(pattern, Locale.getDefault())

fun String?.parseDate(): String {
    var birthday = EMPTY_BIRTHDAY
    this?.let {
        when {
            matches(REGEX_YYYY_MM_DD.toRegex()) -> {
                val date = getSimpleDateFormat(YYYY_MM_DD).parse(this)
                birthday = "${getSimpleDateFormat(SHOW_FORMAT).format(date)} г."
            }
            matches(REGEX_MM_DD_YYYY.toRegex()) -> {
                val date = getSimpleDateFormat(DD_MM_YYYY).parse(this)
                birthday = "${getSimpleDateFormat(SHOW_FORMAT).format(date)} г."
            }
        }
    }
    return birthday
}

fun String?.parseAge(): Int {
    var age = EMPTY_AGE
    val year = Calendar.getInstance().get(Calendar.YEAR)
    this?.let {
        when {
            matches(REGEX_YYYY_MM_DD.toRegex()) -> {
                val date = getSimpleDateFormat(YYYY_MM_DD).parse(this)
                age = year - getSimpleDateFormat(YYYY).format(date).toInt()
            }
            matches(REGEX_MM_DD_YYYY.toRegex()) -> {
                val date = getSimpleDateFormat(DD_MM_YYYY).parse(this)
                age = year - getSimpleDateFormat(YYYY).format(date).toInt()
            }
        }
    }
    return age
}

/**Int*/
fun Int.isAgeEmpty() = this == EMPTY_AGE

/**View*/
fun View?.gone() {
    this?.let { visibility = View.GONE }
}