package com.example.testtask.utils

const val ONE_FRAGMENT = 1
const val DATA_BASE_NAME = "test_task_db"

const val EMPTY_STRING = " "
const val EMPTY_BIRTHDAY = "-"
const val EMPTY_AGE = 0

const val SPECIALTY = "specialty"
const val USER = "user"

const val ERROR = "Error"