package com.example.testtask.utils

const val YYYY_MM_DD = "yyyy-MM-dd"
const val REGEX_YYYY_MM_DD = "([0-9]{4}[-]{1}[0-9]{2}[-]{1}[0-9]{2})"

const val DD_MM_YYYY = "dd-MM-yyyy"
const val REGEX_MM_DD_YYYY = "[0-9]{2}[-]{1}[0-9]{2}[-]{1}[0-9]{4}"

const val YYYY = "yyyy"

const val SHOW_FORMAT = "dd.MM.yyyy"