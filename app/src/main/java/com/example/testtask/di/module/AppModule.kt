package com.example.testtask.di.module

import android.app.Application
import android.arch.persistence.room.Room
import android.content.Context
import com.example.testtask.data.AppDataManager
import com.example.testtask.data.DataManager
import com.example.testtask.data.db.AppDataBase
import com.example.testtask.data.db.DBHelper
import com.example.testtask.data.db.DBManager
import com.example.testtask.data.network.NetworkHelper
import com.example.testtask.data.network.NetworkManager
import com.example.testtask.utils.DATA_BASE_NAME
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(private val application: Application) {

    @Provides
    fun provideContext(): Context = application

    @Provides
    fun provideNetworkHelper(networkManager: NetworkManager): NetworkHelper = networkManager

    @Provides
    fun provideAppData(appDataManager: AppDataManager): DataManager = appDataManager

    @Singleton
    @Provides
    fun provideDBHelper(appDataBase: AppDataBase): DBHelper = DBManager(appDataBase)

    @Singleton
    @Provides
    fun provideAppDataBase() = Room
            .databaseBuilder(provideContext(), AppDataBase::class.java, DATA_BASE_NAME)
            .fallbackToDestructiveMigration()
            .build()
}