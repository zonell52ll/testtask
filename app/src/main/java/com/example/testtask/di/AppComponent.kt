package com.example.testtask.di

import com.example.testtask.di.module.AppModule
import com.example.testtask.di.module.NetworkModule
import com.example.testtask.ui.activity.main.MainPresenter
import com.example.testtask.ui.fragment.specialty.SpecialtyPresenter
import com.example.testtask.ui.fragment.user_info.UserInfoPresenter
import com.example.testtask.ui.fragment.user_list.UserListPresenter
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class, NetworkModule::class])
interface AppComponent {
    fun inject(presenter: MainPresenter)
    fun inject(presenter: SpecialtyPresenter)
    fun inject(presenter: UserListPresenter)
    fun inject(presenter: UserInfoPresenter)
}